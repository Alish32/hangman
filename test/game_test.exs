defmodule GameTest do
    use ExUnit.Case
    import Mock
  
    test "Creates a process holding the game state" do
      with_mock Dictionary, [random_word: fn() -> "platypus" end] do
        {:ok, pid} = Game.start_link()
        assert is_pid(pid)
        assert pid != self()
        assert called Dictionary.random_word
      end
    end

    test "Test 2" do
        with_mock Dictionary, [random_word: fn() -> "platypus" end] do
          {:ok, pid} = Game.start_link()
          assert :ok = Game.submit_guess(pid, "h")
        end
    end

    test "Test 3" do
        with_mock Dictionary, [random_word: fn() -> "word" end] do
          {:ok, pid} = Game.start_link()
          Game.submit_guess(pid, "o")
          assert %{feedback: "-o--", remaining_turns: 8, status: :playing} = Game.get_feedback(pid)
        end
    end

    test "Test 4" do
      with_mock Dictionary, [random_word: fn() -> "word" end] do
        {:ok, pid} = Game.start_link()
        Game.submit_guess(pid, "w")
        Game.submit_guess(pid, "o")
        Game.submit_guess(pid, "r")
        Game.submit_guess(pid, "d")
        assert %{feedback: "word", remaining_turns: 5, status: :win} = Game.get_feedback(pid)
      end
  end


  test "Test 5" do
    with_mock Dictionary, [random_word: fn() -> "word" end] do
      {:ok, pid} = Game.start_link()
      Game.submit_guess(pid, "t")
      Game.submit_guess(pid, "u")
      Game.submit_guess(pid, "j")
      Game.submit_guess(pid, "k")
      Game.submit_guess(pid, "q")
      Game.submit_guess(pid, "u")
      Game.submit_guess(pid, "y")
      Game.submit_guess(pid, "p")
      Game.submit_guess(pid, "m")
      Game.submit_guess(pid, "z")
      assert %{feedback: "----", remaining_turns: 0, status: :lose} = Game.get_feedback(pid)
    end
end
    
    # some other tests follow
  end