defmodule GameRouterTest do
    use ExUnit.Case
    use Plug.Test
  
    @opts GameRouter.init([])
  
    test "Reports path / is not served by the application" do
      conn = conn(:get, "/")
      conn = GameRouter.call(conn, @opts)
      assert conn.status == 404
      assert conn.resp_body == "Oops"
    end

    test "Test 1" do
        assert {:ok, _} = GameRouter.start_link()
      end

    test "Test 2" do
        conn = conn(:post, "/games")
        conn = GameRouter.call(conn, @opts)
    end

    test "Test 3" do
        conn = conn(:get, "/games/8")
        conn = GameRouter.call(conn, @opts)
    end

    test "Test 4" do
        conn = conn(:post, "/games/8/guesses")
        conn = GameRouter.call(conn, @opts)
    end
  end