defmodule Hangman do
    def score_guess({word, corrects, wrongs, remaining}, guess) do
        
        remaining = if(String.contains?(corrects, guess) == false and String.contains?(wrongs, guess) == false and remaining>0) do remaining - 1
        else remaining
        end

        corrects = if(String.contains?(word, guess) and String.contains?(corrects, guess) == false and String.contains?(wrongs, guess) == false and remaining>0) do
            corrects <> guess
        else
            corrects <> ""
        end

        wrongs = if(String.contains?(word, guess) == false and String.contains?(corrects, guess) == false and String.contains?(wrongs, guess) == false and remaining>0) do
            wrongs <> guess
        else
            wrongs <> ""
        end
        
        {word, corrects, wrongs, remaining}
    end

    def format_feedback({word, corrects, _, _}) do
        Enum.join(Enum.map(String.graphemes(word), fn x -> if(String.contains?(corrects, x)) do x else "-" end end))
    end
end
